REPORT		= internship_report
PRESENTATION	= internship_presentation

REPORT_MAIN	= report_src/main.tex
PRESENTATION_MAIN = main.tex

LC		= pdflatex --enable-pipes --shell-escape $(LC_ARGS)

all:		$(PRESENTATION) #$(REPORT)

$(REPORT):
	echo -n '' | $(LC) --jobname="$@" $(REPORT_MAIN)
	makeglossaries "$@"
	echo -n '' | $(LC) --jobname="$@" $(REPORT_MAIN)
	makeglossaries "$@"
	echo -n '' | $(LC) --jobname="$@" $(REPORT_MAIN)
	makeglossaries "$@"
	echo -n '' | $(LC) --jobname="$@" $(REPORT_MAIN)
	makeglossaries "$@"
	#pdftotext -layout $(REPORT).pdf
	echo -n '' | $(LC) --jobname="$@" $(REPORT_MAIN)
	#pdftotext -layout $(REPORT).pdf

$(PRESENTATION):
	echo -n '' | $(LC) --jobname="$@" $(PRESENTATION_MAIN)
	echo -n '' | $(LC) --jobname="$@" $(PRESENTATION_MAIN)
	echo -n '' | $(LC) --jobname="$@" $(PRESENTATION_MAIN)

clean:
	find `dirname "$0"`	\
		-name "*.aux" -type f -delete -o	\
		-name "*.log" -type f -delete -o	\
		-name "*.lof" -type f -delete -o	\
		-name "*.lot" -type f -delete -o	\
		-name "*.toc" -type f -delete -o	\
		-name "*.glg" -type f -delete -o	\
		-name "*.glo" -type f -delete -o	\
		-name "*.gls" -type f -delete -o	\
		-name "*.ist" -type f -delete -o	\
		-name "*.nav" -type f -delete -o	\
		-name "*.snm" -type f -delete -o	\
		-name "*.out" -type f -delete
	rm -f stats charcount wordcount

fclean:	clean
	rm -f $(REPORT).pdf $(REPORT).txt
	rm -f $(PRESENTATION).pdf $(PRESENTATION).txt

re:	fclean all

test:
	./test

.PHONY:	all clean fclean re test
